(function() {
    
  // Localize jQuery variable
  var jQuery;
  
  /******** Load jQuery if not present *********/
  if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.4.2') {
      var script_tag = document.createElement('script');
      script_tag.setAttribute("type","text/javascript");
      script_tag.setAttribute("src",
          "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
      if (script_tag.readyState) {
        script_tag.onreadystatechange = function () { // For old versions of IE
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                scriptLoadHandler();
            }
        };
      } else {
        script_tag.onload = scriptLoadHandler;
      }
      // Try to find the head, otherwise default to the documentElement
      (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
  } else {
      // The jQuery version on the window is the one we want to use
      jQuery = window.jQuery;
      main();
  }
  
  /******** Called once jQuery has loaded ******/
  function scriptLoadHandler() {
      // Restore $ and window.jQuery to their previous values and store the
      // new jQuery in our local jQuery variable
      jQuery = window.jQuery.noConflict(true);
      // Call our main function
      main(); 
  }
  
  /******** Our main function ********/
  function main() { 
      jQuery(document).ready(function($) { 
          /******* Load CSS *******/
  
          var css_link_1 = $("<link>", { 
              rel: "stylesheet", 
              type: "text/css", 
              href: "https://wajooba.xyz/release/build/styles/css/sysbundle-4ec8aeb4.css" 
          });
  $(css_link_1).appendTo('head');
          var css_state1=$("<link>", { 
              rel: "stylesheet", 
              type: "text/css", 
              href: "https://cdnjs.cloudflare.com/ajax/libs/cleanslate/0.10.1/cleanslate.min.css" 
          });
  
          $(css_state1).appendTo('head');
  
         var css_link_2 = $("<link>", { 
              rel: "stylesheet", 
              type: "text/css", 
              href: "https://wajooba.xyz/release/build/styles/css/appbundle-86fef175.css" 
          });
          
          $(css_link_2).appendTo('head');
  
         var css_link_3 = $("<link>", { 
              rel: "stylesheet", 
              type: "text/css", 
              href: "https://wajooba.xyz/release/build/styles/css/schedule-a-0753c508.css" 
          });
           
          $(css_link_3).appendTo('head');
  
          /*css_link.appendTo('head'); */
  
          /*********load js file***********/ 
            
            /*var js_link='';*/
  
  
           var   js_link_1 = $("<link>", { 
              type: "text/javascript", 
              href: "https://wajooba.xyz/release/build/vendor-0cb86258.js" 
          });
  
           $(js_link_1).appendTo('head');
          
          var js_link_2 = $("<link>", {  
              type: "text/javascript", 
              href: "https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js" 
          });
  
          /*console.log(js_link_2);*/
          
          $(js_link_2).appendTo('head');
  
          var js_link_3 = $("<link>", {  
              type: "text/javascript", 
              href: "https://wajooba.xyz/release/build/app-9b43470f.js" 
          });
          
          $(js_link_3).appendTo('head');
  
  
          var js_link_4 = $("<link>", {  
              type: "text/javascript", 
              href: "https://wajooba.xyz/release/build/vendor.ui-ce9b4158.js" 
          });
  
         $(js_link_4).appendTo('head');
  
  
          /*js_link.appendTo('head');*/
          /*$("<link>", {  
              type: "text/javascript", 
              href: "https://wajooba.xyz/release/build/vendor.ui-ce9b4158.js" ,
          }).appendTo( "head" );*/
  
  
  
  
  
          /******* Load HTML *******/
          var jsonp_url = "/project/hello.html";
          $.getJSON(jsonp_url, function(data) {
              
              $('#orgid').show();
            $('#orgid').html(data.three);
          });
      });
  }
  
  })(); // We call our anonymous function immediately